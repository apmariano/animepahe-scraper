# How to use this program
This program was made using Python 3.5.

## Prerequisites

```
pip install pipenv
pipenv install
```

## Using the program
```
pipenv shell
python3 anime.py
```

## Exiting pipenv
Ctrl+D